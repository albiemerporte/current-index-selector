
from PyQt6 import uic
from PyQt6.QtWidgets import QApplication

class Main:
    def __init__(self):
        self.mainformui = uic.loadUi('indextab.ui')
        
        self.mainformui.pb1.clicked.connect(self.tab2func)
        self.mainformui.pb2.clicked.connect(self.tab3func)
        self.mainformui.pb3.clicked.connect(self.tab1func)
        self.mainformui.pb4.clicked.connect(self.tab3func)
        self.mainformui.pb5.clicked.connect(self.tab1func)
        self.mainformui.pb6.clicked.connect(self.tab2func)
    
 #   def entrypoint(self):
  #      self.mainformui.show()
    
    def tab1func(self):
        self.mainformui.tabMain.setCurrentIndex(0)
    
    def tab2func(self):
        self.mainformui.tabMain.setCurrentIndex(1)
        
    def tab3func(self):
        self.mainformui.tabMain.setCurrentIndex(2)

if __name__ == '__main__':
    app = QApplication([])
    mymain = Main()
    mymain.mainformui.show()
    app.exec()